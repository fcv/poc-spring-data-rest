package br.fcv.poc_spring_data_rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PocSpringDataRestApplicationTests {

	@Test
	public void contextLoads() {
	}

}
