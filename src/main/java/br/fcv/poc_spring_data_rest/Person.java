package br.fcv.poc_spring_data_rest;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

import static javax.persistence.GenerationType.AUTO;

/**
 * Created by veronez on 30/11/16.
 */
@Entity
public class Person {

	@Id
	@GeneratedValue(strategy = AUTO)
	private Long id;

	private String name;

	private LocalDate birthdate;

	public Person() {
	}

	Person(String name, LocalDate birthdate) {
		this.name = name;
		this.birthdate = birthdate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Person{");
		sb.append("id=").append(id);
		sb.append(", name='").append(name).append('\'');
		sb.append(", birthdate=").append(birthdate);
		sb.append('}');
		return sb.toString();
	}
}
