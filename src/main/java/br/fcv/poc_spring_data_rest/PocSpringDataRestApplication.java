package br.fcv.poc_spring_data_rest;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.stream.Stream;

import static java.time.Month.DECEMBER;
import static java.time.Month.OCTOBER;
import static javax.persistence.GenerationType.AUTO;

@SpringBootApplication
public class PocSpringDataRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocSpringDataRestApplication.class, args);
	}

	@Bean
	public Logger getLogger(InjectionPoint ip) {
		return LoggerFactory.getLogger(ip.getMember().getDeclaringClass());
	}

	@Bean
	public Module newJavaTimeModule() {
		return new JavaTimeModule();
	}

}

@Component
class SampleDataCLR implements CommandLineRunner {

	private final PersonRepository personRepository;
	private final Logger logger;

	public SampleDataCLR(PersonRepository personRepository, Logger logger) {
		this.personRepository = personRepository;
		this.logger = logger;
	}

	@Override
	public void run(String... args) throws Exception {

		Stream.of(new Person("John Doe", LocalDate.of(1980, OCTOBER, 27)),
				new Person("Maria Scarlet", LocalDate.of(1981, DECEMBER, 3)))
			.forEach(personRepository::save);

		logger.info("{}", personRepository.findAll());
	}
}

