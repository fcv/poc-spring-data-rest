# POC Spring Data Rest

Simple POC project used to experiment [Spring Data Rest](http://projects.spring.io/spring-data-rest/). This project code is highly based on Spring's Getting Started ["Accessing JPA Data with REST"](https://spring.io/guides/gs/accessing-data-rest/) and Spring Data Rest's own [Reference Documentation page](http://docs.spring.io/spring-data/rest/docs/2.5.5.RELEASE/reference/html/).

# Getting Started

This project requires Java 8 and it uses Maven 3 as build tool. Thus it may be packaged using command

    $ mvn package

It's developed over [Spring Boot](https://projects.spring.io/spring-boot/) and thus it can be executed using following command:

    $ mvn spring-boot:run


System by default uses a in-memory H2 database so there is no requirement on an external DBMS.

After System is up and running REST API is exposed under base path `/api/rest/v1`. Example:

    $ curl -i -X GET "http://localhost:8080/api/rest/v1/"
    HTTP/1.1 200 
    Content-Type: application/hal+json;charset=UTF-8
    Transfer-Encoding: chunked
    Date: Wed, 30 Nov 2016 21:38:43 GMT
    
    {
      "_links" : {
        "people" : {
          "href" : "http://localhost:8080/api/rest/v1/people"
        },
        "profile" : {
          "href" : "http://localhost:8080/api/rest/v1/profile"
        }
      }
    }

Or listing [People](src/main/java/br/fcv/poc_spring_data_rest/Person.java) entities:

    $ curl -i -X GET "http://localhost:8080/api/rest/v1/people"
    HTTP/1.1 200 
    Content-Type: application/hal+json;charset=UTF-8
    Transfer-Encoding: chunked
    Date: Wed, 30 Nov 2016 21:40:34 GMT
    
    {
      "_embedded" : {
        "people" : [ {
          "name" : "John Doe",
          "birthdate" : "1980-10-27",
          "_links" : {
            "self" : {
              "href" : "http://localhost:8080/api/rest/v1/people/1"
            },
            "person" : {
              "href" : "http://localhost:8080/api/rest/v1/people/1"
            }
          }
        }, {
          "name" : "Maria Scarlet",
          "birthdate" : "1981-12-03",
          "_links" : {
            "self" : {
              "href" : "http://localhost:8080/api/rest/v1/people/2"
            },
            "person" : {
              "href" : "http://localhost:8080/api/rest/v1/people/2"
            }
          }
        } ]
      },
      "_links" : {
        "self" : {
          "href" : "http://localhost:8080/api/rest/v1/people"
        },
        "profile" : {
          "href" : "http://localhost:8080/api/rest/v1/profile/people"
        }
      }
    }

Or creating a new Person:

    $ curl -i -X POST \
    > -H "Content-Type: application/json" \
    > -d '{"name": "Charles Dickens", "birthdate": "1812-02-07"}' \
    > "http://localhost:8080/api/rest/v1/people/"
    HTTP/1.1 201 
    Location: http://localhost:8080/api/rest/v1/people/3
    Content-Type: application/hal+json;charset=UTF-8
    Transfer-Encoding: chunked
    Date: Wed, 30 Nov 2016 21:48:14 GMT
    
    {
      "name" : "Charles Dickens",
      "birthdate" : "1812-02-07",
      "_links" : {
        "self" : {
          "href" : "http://localhost:8080/api/rest/v1/people/3"
        },
        "person" : {
          "href" : "http://localhost:8080/api/rest/v1/people/3"
        }
      }
    }

Or deleting one:

    $ curl -i -X DELETE "http://localhost:8080/api/rest/v1/people/2"
    HTTP/1.1 204 
    Date: Wed, 30 Nov 2016 21:49:31 GMT
